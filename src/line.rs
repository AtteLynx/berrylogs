use chrono::{DateTime, TimeZone, Utc};
use lazy_static::lazy_static;
use regex::Regex;
use std::str::SplitWhitespace;

use crate::types::{Emote, Video};

lazy_static! {
    static ref RE_LINE: Regex = Regex::new(
        r"(?x)
        ^
        (?P<year>\d{4})-(?P<month>\d{2})-(?P<day>\d{2})
        \s
        (?P<hour>\d{2}):(?P<minute>\d{2}):(?P<second>\d{2})
        \s+
        (?P<mode>[~&@+%])?
        (?P<nick>\S+)
        \s+
        (?P<message>[^\r\n]+)
    "
    )
    .expect("Invalid line regex");
    static ref RE_EMOTE: Regex = Regex::new(
        r"(?x)
        \[(?P<text>[^\]]*)\]
        \(/
            (?P<name>[\w:!\#/]+)
            (?P<flags>[\w!-]*)
            ([^\)]*)
        \)
    "
    )
    .expect("Invalid emote regex");
    static ref RE_LINK: Regex = Regex::new(
        r"(?x)
        https?://[^\s,]+
    "
    )
    .expect("Invalid line regex");
}

#[derive(Debug)]
pub struct Line {
    pub time: DateTime<Utc>,
    pub mode: Option<char>,
    pub nick: String,
    pub message: String,
}

impl Line {
    pub fn parse(line: &str) -> Option<Self> {
        let caps = RE_LINE.captures(line)?;
        Some(Self {
            time: Utc
                .ymd(
                    caps.name("year")?.as_str().parse().ok()?,
                    caps.name("month")?.as_str().parse().ok()?,
                    caps.name("day")?.as_str().parse().ok()?,
                )
                .and_hms(
                    caps.name("hour")?.as_str().parse().ok()?,
                    caps.name("minute")?.as_str().parse().ok()?,
                    caps.name("second")?.as_str().parse().ok()?,
                ),
            mode: caps
                .name("mode")
                .and_then(|cap| cap.as_str().chars().next()),
            nick: caps.name("nick")?.as_str().to_owned(),
            message: caps.name("message")?.as_str().to_owned(),
        })
    }

    pub fn emotes(&self) -> Vec<Emote<'_>> {
        RE_EMOTE
            .captures_iter(&self.message)
            .filter_map(|caps| Emote::from_caps(&caps))
            .collect()
    }

    pub fn links(&self) -> Vec<&str> {
        RE_LINK
            .captures_iter(&self.message)
            .map(|caps| caps.get(0).unwrap().as_str())
            .collect()
    }

    pub fn nick_list(&self) -> Result<SplitWhitespace<'_>, &'static str> {
        if !self.message.starts_with("Nicks #berrytube: ") {
            return Err("wrong prefix");
        }

        let index = self.message.find('[').ok_or("no [")?;
        Ok(self.message[index..].split_whitespace())
    }

    pub fn video_change(&self) -> Result<Video, &'static str> {
        if !self
            .message
            .starts_with("BerryTube has changed topic for #berrytube")
        {
            return Err("wrong prefix");
        }

        let video = {
            let index = self
                .message
                .rfind("Now Playing: ")
                .ok_or("no Now Playing")?;
            self.message[index + 13..self.message.len() - 1].to_string()
        };

        let (title, url) = {
            let index = video.rfind(" ( ").ok_or("no opening paren")?;
            (
                video[0..index].to_string(),
                video[index + 3..video.len() - 2].to_string(),
            )
        };

        Ok(Video::new(title, url))
    }
}
