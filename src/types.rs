use regex::Captures;

pub struct Emote<'a> {
    pub name: &'a str,
    pub text: Option<&'a str>,
    pub flags: Option<&'a str>,
}

pub struct Video {
    pub title: String,
    pub url: String,
}

impl<'a> Emote<'a> {
    pub fn from_caps(caps: &Captures<'a>) -> Option<Emote<'a>> {
        caps.name("name").map(|name| Emote {
            name: name.as_str(),
            text: caps.name("text").map(|m| m.as_str()),
            flags: caps.name("flags").map(|m| m.as_str()),
        })
    }
}

impl Video {
    #[inline]
    pub fn new(title: String, url: String) -> Self {
        Self { title, url }
    }
}
