use crate::Line;
use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;
use std::iter;
use std::path::PathBuf;

pub struct Lines {
    paths: Vec<PathBuf>,
    reader: Option<BufReader<File>>,
    buffer: String,
}

impl Lines {
    pub fn from_paths(paths: Vec<PathBuf>) -> Self {
        let mut lines = Self {
            paths,
            reader: None,
            buffer: String::new(),
        };
        lines.next_file();
        lines
    }

    fn next_file(&mut self) {
        while !self.paths.is_empty() {
            let path = self.paths.remove(0);
            match File::open(&path) {
                Ok(file) => {
                    self.reader = Some(BufReader::new(file));
                    return;
                }
                Err(e) => {
                    println!("Error opening {}: {}", path.display(), e);
                }
            }
        }
        self.reader = None;
    }

    fn next_line(&mut self) -> Option<Line> {
        if let Some(reader) = self.reader.as_mut() {
            while reader.read_line(&mut self.buffer).unwrap_or(0) > 0 {
                let parsed = Line::parse(&self.buffer);
                self.buffer.clear();

                if parsed.is_some() {
                    return parsed;
                }
            }
        }
        None
    }
}

impl Iterator for Lines {
    type Item = Line;

    fn next(&mut self) -> Option<Line> {
        while self.reader.is_some() {
            let line = self.next_line();
            if line.is_some() {
                return line;
            }
            self.next_file();
        }
        None
    }
}

impl iter::FusedIterator for Lines {}
