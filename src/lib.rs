#![deny(clippy::all, clippy::pedantic)]
#![allow(clippy::missing_docs_in_private_items, clippy::print_stdout)]

use lazy_static::lazy_static;
use std::{env, path::PathBuf};

mod line;
mod lines;
mod types;

pub use crate::{
    line::Line,
    lines::Lines,
    types::{Emote, Video},
};

lazy_static! {
    static ref LOG_PATH: String =
        env::var("BERRYLOGS_PATH").unwrap_or_else(|_| "./logs".to_string());
    static ref LOG_GLOB: String = env::var("BERRYLOGS_GLOB")
        .unwrap_or_else(|_| format!("{}/irc.berrytube.#berrytube.*.weechatlog", *LOG_PATH));
}

fn get_log_paths() -> Vec<PathBuf> {
    let mut paths: Vec<_> = glob::glob(LOG_GLOB.as_str())
        .expect("Invalid glob")
        .filter_map(|x| x.ok())
        .collect();
    paths.sort();
    paths
}

#[inline]
pub fn lines_n(nfiles: usize) -> Lines {
    let paths = get_log_paths();
    Lines::from_paths(paths[paths.len() - nfiles..].to_vec())
}

#[inline]
pub fn lines() -> Lines {
    let paths = get_log_paths();
    Lines::from_paths(paths)
}
